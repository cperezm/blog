+++
title = "Org mode showcase"
date = 2018-12-05T20:34:00+01:00
lastmod = 2018-12-08T16:48:00+01:00
draft = false
author = "Carlos Perez"
+++

Setting up a simple text-based notebook.


## Org mode structure {#org-mode-structure}

Text in org is structured by headings, denoted by lines starting with \* so we
are currectly on a **section**


### A subheading {#a-subheading}

Starts with an extra \*.


### Lists {#lists}


#### Bullet lists {#bullet-lists}

-   Bullet list can be created with a space and -
-   `M-ret` gives a new bullet
-   Nested bullets
    -   `M-ret` TAB
    -   Now `M-ret` indents to the previous
-   Now TAB two times let you reindent again.
-   The nice thing is that for long lines of text, emacs wraps them so that they
    line up with the bullet.
-   You can reorder bullets like sections with `M-up` `M-down`
-   You can change bullet style with `S-left` `S-right`.


#### Numbered lists {#numbered-lists}

1.  Numbered lists are also possible
2.  M-ret gives a new number
3.


#### Checklists <code>[1/5]</code> {#checklists}

-   [ ] We can even have checklists
-   [ ] M-S-RET gives a new item with checkbox
-   [X] C-c C-c checks unchecks a box
-   [ ] You can have subitems
    -   [ ] Like this
    -   [ ] Can be checked off individually
-   [ ] You can track the number of items by adding <code>[0/0]</code> to the end of a line
    above a checklist - This updates when you check items off


#### Definition lists {#definition-lists}

Next, some defintions.

Definition lists
: they are useful sometimes

item 2
: M-RET gives another item, long lines wrap up in a tidy way when
    using a definition.


## Use simple tables in your notes {#use-simple-tables-in-your-notes}

Adding simple tables to your notes.


### Tables {#tables}

Hopefully you can see right away that the simple structure provided by org-mode
gives a nice way to keep an electronic notebook.

Often it's nice to include tables in our notes. Org handles this by using | to
separate columns, and a line of --- (C-c -) to add horizontal lines.

<span class="underline">Exercise</span>: start typing in this table below.

1.  When you get to the last "s" in comments, press TAB to go to the next line.
2.  Go up to the previous line and use C-c - to add the row of dashes
3.  Next, enter a few lines of data, using TAB to go through the cells - you
    should notice the columns changing width as needed.

**Note:** You can do the first two steps in one by C-c RET after the last "s" in
comments.

| ID | x   | y   | comments |
|----|-----|-----|----------|
| 1  | 24  | 6   | first    |
| 2  | 64  | 24  | second   |
| 3  | 8   | 15  | third    |
| 4  | 23  | 63  | fourth   |
| 5  | 102 | 101 | fifth    |

-   Change order of rows and columns: `M-arrow`
-   Insert or delete rows and columns: `M-S-arrow`


### Creating and exporting tables {#creating-and-exporting-tables}

You can create an empty table using `C-c |` to run the command
`org-table-create-or-convert-from-region` which will prompt for table
dimensions if no region is selected.

The same command can easily convert some text to table. Select the text and use
`C-c |` to run the command `org-table-create-or-convert-from-region` again to
convert text to table.

| ID    | x  | y  | Mean |
|-------|----|----|------|
| A     | 9  | 4  | 6.5  |
| B     | 5  | 4  | 4.5  |
| C     | 18 | 8  | 13   |
| D     | 36 | 16 | 26   |
| Means | 68 | 32 | 50   |

You can also save tables to their own files by putting the cursor in the table
and using `M-x` `org-table-export`. You'll be asked for a file name and a
format. For the format, type orgtbl-to and press TAB to see available options.


### Formulae {#formulae}

You can use formulae to do arithmetic on tables and use them like a
spreadsheet.
One useful command is `C-c +` which runs `org-table-sum` to sum the numbers in
the current column.


## Add links and images to your notes {#add-links-and-images-to-your-notes}

We'll look at adding links and images. Links can be files, URLs or locations
in the current org document. If the link is to an image, then emacs can display
it inline int the org document. This is handy for enhancing your notes and will
also be useful when we come to look at exporting to different formats.


### Links and images {#links-and-images}

Org mode supports links to files, URLs and to other points in the org file. In
this example, let's use an image from my website. First copy it to the current
directory. Run this command:

{{< highlight bash >}}
curl http://www.star.bris.ac.uk/bjm/superman_cluster.gif -o superman_cluster.gif
{{< /highlight >}}

To add a link to a file in the same directory use `C-u C-c C-l` and type the
name of the file. Use tab-completion to select the image we just copied and you
will then be asked for a description - you can press ENTER to leave this
blank. This will create a link that looks like this

{{< figure src="/ox-hugo/superman_cluster.gif" link="/ox-hugo/superman_cluster.gif" >}}

If you do this in your org file, it appears as a clickable link.

Since the file we have linked to is an image, we can tell emacs to show the
image inline in the document using `C-c C-x C-v` and use the same command to
turn it off.

You can also click the link with the mouse, or use `C-c C-o` to follow it,
which might open your web browser, an image viewer or an emacs buffer.

The structure of a link in org mode looks like this

```text
[[link address][description]]
```

The example block allows to show the structure of an org link.
The link address is the URL or file name and the description is the text that
is displayed, so we can replace our superman link with something tidier like

```text
[[file:superman_cluster.gif][this]]
```

Links to web pages are easy - just put the http address in as the link
address. Use `C-c C-l` as a quick way to add such a link (Remember we used `C-u
C-c C-l` for adding a link to a file).

Links to other parts of the org file are added easily like

```text
[[Links and images][this link]]
```

Because the address part of the link matches a headline in this document, then
org-mode points the link to that part of he file. Clickint it will move the
cursor there.

Finally, we can add a caption and a name to our image like this

<a id="orgb0c6a8c"></a>

{{< figure src="/ox-hugo/superman.jpg" caption="Figure 1: Superman and a galaxy cluster" link="/ox-hugo/superman.jpg" >}}

which means we can refer to our image later with a link like this one.
[1](#orgb0c6a8c)


## Formating text {#formating-text}

We will look at formatting the text in your notes.


### Simple formatting {#simple-formatting}

You can apply simple formatting in your text by enclosing words in special
characters. These include

-   _italicised text_
-   **bold text**
-   <span class="underline">underlines</span>
-   `literal text`
-   `code` (generally appears the same as literal text)


### Formatted blocks of text {#formatted-blocks-of-text}

For longer pieces of text you can enclose it in blocks that mark specific sort
of text. I commonly use these:

```text
This is an example block into which you can type text that you don't want org
to mess with like [[link]]. This will typically be rendered in a monospace font
when exported.
```

> This block encloses text that you want to appear as a quotation

<style>.org-center { margin-left: auto; margin-right: auto; text-align: center; }</style>

<div class="org-center">
  <div></div>

This text will be centered when exported

</div>

You can use shortcuts to introduce blocks. Go to the start of a new line and
type <e and press TAB and it will expand to an example work. The same happens
with <q and <c


### \LaTeX {#latex}

Org mode does a good job when understanding snippets of LaTeX ([powerful
typesetting language](https://www.latex-project.org/) used in scientific and other technical documents). For
example, it will correctly export single superscripts like x^2 or subscripts
x\_0 or symbols like &alpha;, &beta;, &gamma;.

Org also understand more complex \LaTeX{} like this:

\\[ x^2 + \left(\frac{y}{z}\right)^4 = 0 \\]

but for longer bits of LaTeX it is better to use a LaTeX block. You start one
with <l and TAB

<div class="LaTeX">
  <div></div>

\LaTeX formatted equation: \\( E = -J \sum\_{i=1}^N s\_i s\_{i+1} \\)

</div>


### Source code blocks {#source-code-blocks}

It is also handy to include source code in you notes - on a new line type <s
and TAB to create a source block. You can tell org what type of code is
contained - in this case we'll put some simple shell code , so we'll put "sh"
at the top of the block.

{{< highlight sh >}}
echo "Hello $USER! Today is `date` "
exit
{{< /highlight >}}

```text
Hello cperezm! Today is Mi 15. Apr 19:36:40 CEST 2020
```

You can get org to syntax highlight the text in the block by adding the
following to your emacs config file:

{{< highlight elisp >}}
;;syntax highlight code blocks
(setq org-src-fontify-natively t)
{{< /highlight >}}

What is more, when the cursor is inside a SRC block, you can use C-c ' to
create a new temporary buffer in the major mode of the programming language you
have specified. Type some code in, and then type C-c ' again to come back to
this buffer.

{{< highlight latex >}}
Some \LaTeX{} code
{{< /highlight >}}


### Executing source code blocks {#executing-source-code-blocks}

Org mode can execute your source code blocks and add the output to your
file. This part of org mode is called org-babel.

For example, take the simple code block we had above. Put the cursor inside the
block and hit C-c C-c to execute it. You will be asked to confirm and then you
should see the output appear.


## Navigation {#navigation}

TAB: Headings can be expanded or collapsed.
S-TAB: cycles all headings
C-c C-n | C-c C-p: Move between headings

M-up | M-down: Reorder headings
M-left M-right: Change level of headings
M-S-left M-S-right: Change level of headings and its subheadings


## Code {#code}

{{< highlight python >}}
import numpy
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(4, 2))
x = numpy.linspace(-15, 15)
plt.plot(numpy.cos(x)/x)
fig.tight_layout()
plt.savefig('img/python-matplot-fig.png')
{{< /highlight >}}

{{< figure src="/ox-hugo/python-matplot-fig.png" >}}

+++
title = "Homepage"
draft = false
author = "Carlos Perez"
+++

This website is powered by [Gitlab pages](https://about.gitlab.com/product/pages/), [Hugo](https://gohugo.io/) and can be built in under 1
minute. Literally. It uses the `beautifulhugo` theme which supports content on
your front page. This is all.
